EP1 ORIENTACAO A OBJETOS - FGA

O trabalho Ep1 consiste na criacao de um jogo, estilo batalha naval, utilizando a linguagem C++ orientada a objetos. Infelizmente o projeto nao ficou pronto em sua totalidade, deixando varios requisitos obrigatorios a serem feitos, portanto, nao esta funcionando.

Dentre os requisitos pedidos, a criacao dos mapas(matriz oculta e printada) e classes (barcos, submarinos, porta-avioes, canoas) foram realizadas. A main possui bugs devido o mal funcionamentos de algumas classes.

*Mesmo nao completando o trabalho, muitas horas foram investidas em tal e muito bagagem de aprendizado foi embarcada. Todas as partes do codigo foram comentadas para uma melhor avaliacao.

*********** Plano de trabalho **************
1.Instanciar todas as classes (mapa, mapa print, barcos, submarino, canoa) -> feito.
2.Implementar funcoes de input e output pelo usuario -> feito parcialmente e com erros.
3.Codificar a main de acordo com a wiki -> feito parcialmente e com erros.
