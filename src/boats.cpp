#include <iostream>
#include <string>

#include "boats.hpp"

Boats::Boats(){
    string name = "";
    int size = 0;
    int power_attack = 0;
    int life = 0;
    int position_l = 0;
    int position_c = 0;
    string postition = position_c + " - " + position_l;
    cout << "Embarcacao construida com sucesso!\n" << endl;
}

Boats::~Boats(){
    cout << "Embarcacao destruida\n" << endl;
}
//Nome
string Boats::get_name(){
    cout << name + "\n" << endl;
    return name;
}

void Boats::set_name(string name){
    this -> name = name;
}
//Tamanho
int Boats::get_size(){
    cout << size + "\n" << endl;
    return size;
}

void Boats::set_size(int size){
    this -> size = size;
}
//Poder de ataque
int Boats::get_power_attack(){
    cout << power_attack + "\n" << endl;
    return power_attack;
}

void Boats::set_power_attack(int power_attack){
    this -> power_attack = power_attack;
}
//Vida
int Boats::get_life(){
    cout << life + "\n" << endl;
    return life;
}

void Boats::set_life(int life){
    this -> life = life;
}
//Posicao
int Boats::get_position_c(){
    cout << position_c + "\n" << endl;
    return position_c;
}

void Boats::set_position_c(int position_c){
    this -> position_c = position_c;
}

int Boats::get_position_l(){
    cout << position_l + "\n" << endl;
    return position_l;
}

void Boats::set_position_l(int position_l){
    this -> position_l = position_l;
}

string Boats::get_position(){
    cout << position + "\n" << endl;
    return position;
}

void Boats::set_position(string position){
    this -> position = position_c + "-" + position_l;
}
//Print All
string Boats::print_all(){
    cout << "********************************" << endl;
    cout << "Nome: " << get_name() << endl;
    cout << "Tamanho: " << get_size() << endl;
    cout << "Poder de ataque: " << get_power_attack() << endl;
    cout << "Vida: " << get_life() << endl;
    cout << "Posicao" << get_position() << endl;
    cout << "********************************" << endl;
}

