#ifndef BOATS_HPP
#define BOATS_HPP

#include <iostream>
#include <string>

using namespace std;

class Boats {
    private:
            //Atributos
        string name;
        int size;
        int power_attack;
        int life;
        int position_l;
        int position_c;
        string position;

    public:
        //Construtores
        Boats();
        ~Boats();

        //Nome
        string get_name();
        void set_name(string name);
        //Tamanho
        int get_size();
        void set_size(int size);

        //Poder de ataque
        int get_power_attack();
        void set_power_attack(int power_attack);

        //Vida
        int get_life();
        void set_life(int life);

        //Posicao
        int get_position_c();
        void set_position_c(int position_c);
        int get_position_l();
        void set_position_l(int position_l);
        string get_position();
        void set_position(string position);

        //Imprimir tudo
        string print_all();
};

#endif