#include <iostream>
#include <string>

#include "mapa_print.hpp"

//Constroi
Mapa_print::Mapa_print(){

    
    //Define o resto do mapa como 0 
    for(int i=0; i<14; i++){
        for(int j=0; j<13; j++){
                map_print[i][j] = '0';
            }
        };

    //Define label superior
    for(int x=0; x<13; x++){
        cout << "(" << x+1 << ")" << "***"; 
    }
    cout << "\n";
    

    //Printa o mapa pronto
    for(int a=0; a<13; a++){
        for(int b=0; b<13; b++){
           cout << map_print[a][b] << "  -  ";    
        };
        //Define label lateral
        cout << "(" << a+1 << ")" << "\n";
    };
}

//Destrutor
Mapa_print::~Mapa_print(){
    cout << "Mapa destruido com sucesso!\n";
}

