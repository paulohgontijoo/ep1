#include <iostream>
#include <string>

#include "submarine.hpp"

Submarine::Submarine(){
        set_name("Submarino");
        set_size(2);
        set_life(2);
        set_position_l(0);
        set_position_c(0);
        //set_position(get_position_c, get_position_l);
        cout << "Submarino construido com sucesso!\n" << endl;
}
Submarine::~Submarine(){
    cout << "Submarino destruido com sucesso!\n" << endl;
}
