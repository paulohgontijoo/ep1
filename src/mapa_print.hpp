#ifndef MAPA_PRINT_HPP
#define MAPA_PRINT_HPP

#include <string>


using namespace std;

class Mapa_print{
    private:
        int lin_antes;
        int col_antes;
        int lin;
        int col;
        int lin_post;
        int col_post;
        string map_print[14][14];

    public:
        //Construtores
        Mapa_print();
        ~Mapa_print();

};
#endif