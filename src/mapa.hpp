#ifndef MAPA_HPP
#define MAPA_HPP


#include <iostream>
#include <string>

using namespace std;

class Mapa{
    private:
        int lin;
        int col;
        
        string mapa[13][13];

    public:
        //Construtores
        Mapa();
        ~Mapa();

        //Infos de mapa
            //Percorre a matriz mapa
        void choose_map_place(int lin, int col);
            //Printa a info
        string get_map_info();
            //Muda a info
        int change_map_info(int lin, int col);

            //JOGADAS
        void play();
};

#endif